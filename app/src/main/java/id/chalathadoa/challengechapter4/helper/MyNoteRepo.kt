package id.chalathadoa.challengechapter4.helper

import android.content.Context
import id.chalathadoa.challengechapter4.database.MyNoteDatabase
import id.chalathadoa.challengechapter4.database.Note
import id.chalathadoa.challengechapter4.database.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MyNoteRepo(context: Context) {
    private val mDb = MyNoteDatabase.getInstance(context)

    private lateinit var user : User

    suspend fun getDataUser() = withContext(Dispatchers.IO){
        mDb?.userDao()?.getAllUser()
    }

    suspend fun getFull(userEmail: String, username: String, password: String) = withContext(Dispatchers.IO){
        mDb?.userDao()?.getRegisUser(userEmail, username, password)
    }

    suspend fun getEmail(userEmail: String) = withContext(Dispatchers.IO){
        mDb?.userDao()?.getEmail(userEmail)
    }

    suspend fun getUsername(username: String) = withContext(Dispatchers.IO){
        mDb?.userDao()?.getUsername(username)
    }

    suspend fun getPsw(password: String) = withContext(Dispatchers.IO){
        mDb?.userDao()?.getPsw(password)
    }

    suspend fun addDataUser(uer: User) = withContext(Dispatchers.IO){
        mDb?.userDao()?.insertUser(user)
    }

    suspend fun updateUser(user: User) = withContext(Dispatchers.IO){
        mDb?.userDao()?.updateUser(user)
    }

    suspend fun deleteUser (user: User) = withContext(Dispatchers.IO){
        mDb?.userDao()?.deleteUser(user)
    }

    //crud Note
    suspend fun insertNote(note: Note) = withContext(Dispatchers.IO){
        val note = Note(noteId = null, userEmail = user.userEmail, title = note.title, body = note.body)
        mDb?.myNoteDao()?.insertNote(note)
    }

    suspend fun updateNote(note: Note) = withContext(Dispatchers.IO){
        val note = Note(noteId = null, userEmail = user.userEmail, title = note.title, body = note.body)
        mDb?.myNoteDao()?.updateNote(note)
    }

    suspend fun deleteNote(note: Note) = withContext(Dispatchers.IO){
        val note = Note(noteId = null, userEmail = user.userEmail, title = note.title, body = note.body)
        mDb?.myNoteDao()?.deleteNote(note)
    }

    suspend fun getNote(note: Note) = withContext(Dispatchers.IO){
        val note = Note(noteId = null, userEmail = user.userEmail, title = note.title, body = note.body)
        mDb?.myNoteDao()?.getAllNote(emailUser = user.userEmail)
    }

    suspend fun getData(): List<Note> {
        var result = emptyList<Note>()
        CoroutineScope(Dispatchers.IO).launch {
            result = mDb?.myNoteDao()?.getAllNote(emailUser = user.userEmail)!!
        }
        return result
    }
}
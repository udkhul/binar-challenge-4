package id.chalathadoa.challengechapter4.database

import androidx.room.*

@Dao
interface MyNoteDao {
    //mendapatkan note sesuai user
    @Query("SELECT * FROM Note WHere user_email = :emailUser")
    fun getAllNote(emailUser: String) : List<Note>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNote(note: Note) : Long

    @Update
    fun updateNote(note: Note) : Int

    @Delete
    fun deleteNote(note: Note) : Int


}
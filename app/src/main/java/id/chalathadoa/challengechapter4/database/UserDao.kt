package id.chalathadoa.challengechapter4.database

import androidx.room.*

@Dao
interface UserDao {
    @Query("SELECT * FROM User")
    fun getAllUser() : List<User>

    @Query("SELECT * FROM User WHERE userEmail = :userEmail AND username = :username AND password = :password LIMIT 1")
    fun getRegisUser(userEmail: String, username: String, password: String) : List<User>

    @Query("SELECT userEmail FROM User WHERE userEmail = :userEmail")
    fun getEmail(userEmail: String?): String

    @Query("SELECT password FROM User WHERE password = :password")
    fun getPsw(password: String?): String

    @Query("SELECT username FROM User WHERE username = :username")
    fun getUsername(username: String?): String

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Long

    @Update
    fun updateUser(user: User): Int

    @Delete
    fun deleteUser(user: User): Int
}